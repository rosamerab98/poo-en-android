package facci.pm.ta2.rosamera4C.datalevel;


public interface GetCallback<DataObject> {
    public void done(DataObject object, DataException e);
}
