package facci.pm.ta2.rosamera4C.datalevel;

import java.util.ArrayList;


public interface FindCallback<DataObject> {
    public void done(ArrayList<DataObject> objects, DataException e);
}
