package facci.pm.ta2.rosamera4C.pra1;

import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.widget.ImageView;
import android.widget.TextView;

import facci.pm.ta2.rosamera4C.datalevel.DataException;
import facci.pm.ta2.rosamera4C.datalevel.DataObject;
import facci.pm.ta2.rosamera4C.datalevel.DataQuery;
import facci.pm.ta2.rosamera4C.datalevel.GetCallback;

public class DetailActivity extends AppCompatActivity {

    public ResultsActivity m_adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("PR1 :: Detail");


        // INICIO - CODE6

        //3.1. Aqui se accede al object_id que se ha recibido como el parámetro para la actividad.
        String object_id = getIntent().getStringExtra("object_id");

        //3.2. Se usa el setMovementMetthod
        TextView descripcion = (TextView) findViewById(R.id.description);
        descripcion.setMovementMethod(LinkMovementMethod.getInstance());

        //3.3 Aqui se accede a las propiedades del object de tipo String
        DataQuery query = DataQuery.get("item");
        query.getInBackground(object_id, new GetCallback<DataObject>() {
            @Override
            public void done(DataObject object, DataException e) {
                if (e == null) {
                    ImageView image = (ImageView)findViewById(R.id.thumbnail);
                    TextView name = (TextView)findViewById(R.id.name);
                    TextView price = (TextView)findViewById(R.id.price);
                    TextView descripcion = (TextView)findViewById(R.id.description);

                    //3.4.
                    image.setImageBitmap ((Bitmap) object.get("image"));
                    name.setText((String) object.get("name"));
                    price.setText((String) object.get ("price")+"\u0024");    //Se añade el codigo unicode del signo de $
                    descripcion.setText((String) object.get("description"));

                }else{
                    //Error
                }
            }
        });

        // FIN - CODE6

    }

}
